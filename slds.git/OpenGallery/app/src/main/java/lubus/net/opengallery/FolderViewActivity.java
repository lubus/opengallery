/*
* Copyright 2015 Eicke Hauck
*
* This file is part of OpenGallery.
*
* OpenGallery is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* OpenGallery is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenGallery.  If not, see <http://www.gnu.org/licenses/>.
*/


package lubus.net.opengallery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;

import lubus.net.opengallery.helper.GlobalMethods;

public class FolderViewActivity extends AppCompatActivity {

    PictureFolder folder;
    GridView gridview;
    FolderImageAdapter imageAdapter;
    int columnWidth;

    public final static String EXTRA_PICTURE_PATH = "lubus.net.opengallery.PICTURE_PATH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // receive Intent
        Intent intent = getIntent();
        this.folder = intent.getParcelableExtra(MainViewActivity.EXTRA_FOLDER);

        setContentView(R.layout.main_grid);
        getSupportActionBar().setTitle(folder.getFolder().getName()); // set title
        getSupportActionBar().setDisplayHomeAsUpEnabled(false); // disable up button, because it forces MainView to reload


        gridview = (GridView) findViewById(R.id.gridview);

        this.columnWidth = GlobalMethods.initializeGridLayout(this, gridview);

        this.imageAdapter = new FolderImageAdapter(this, this.columnWidth);
        gridview.setAdapter(this.imageAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Intent pictureViewIntent = new Intent(FolderViewActivity.this, PictureViewActivity.class);
                pictureViewIntent.putExtra(EXTRA_PICTURE_PATH, folder.getPicturePaths().get(position));
                startActivity(pictureViewIntent);
            }
        });

        refreshViewContents();
    }


    /**
     * Reload images and refresh FolderView
     */
    public void refreshViewContents() {
        imageAdapter.addThumbs(this.folder.getPicturePaths());
    }

}
