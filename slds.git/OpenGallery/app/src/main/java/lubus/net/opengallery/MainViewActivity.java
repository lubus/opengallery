/*
* Copyright 2015 Eicke Hauck
*
* This file is part of OpenGallery.
*
* OpenGallery is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* OpenGallery is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenGallery.  If not, see <http://www.gnu.org/licenses/>.
*/


package lubus.net.opengallery;

import android.Manifest;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import lubus.net.opengallery.helper.GlobalConstants;
import lubus.net.opengallery.helper.GlobalMethods;
import lubus.net.opengallery.helper.PermissionManager;

public class MainViewActivity extends AppCompatActivity {

    private ArrayList<PictureFolder> folders = new ArrayList<>();
    private MainImageAdapter imageAdapter;
    private GridView gridview;
    private int columnWidth;
    private FileCrawler fileCrawler;


    public final static String EXTRA_FOLDER = "lubus.net.opengallery.FOLDER";
    public final static String EXTRA_COLUMN_WIDTH = "lubus.net.opengallery.COLUMN_WIDTH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_grid);

        //GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview = (GridView) findViewById(R.id.gridview);
        this.columnWidth = GlobalMethods.initializeGridLayout(this, gridview);


        this.imageAdapter = new MainImageAdapter(this, columnWidth);
        gridview.setAdapter(this.imageAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Intent intent = new Intent(MainViewActivity.this, FolderViewActivity.class);
                intent.putExtra(EXTRA_FOLDER, folders.get(position));
                intent.putExtra(EXTRA_COLUMN_WIDTH, columnWidth);

                startActivity(intent);
            }
        });

        crawlFileSystem();
    }


    /**
     * Search for pictures in file system
     **/
    private void crawlFileSystem() {
        PermissionManager.setActivity(this);
        PermissionManager.verifyStoragePermissions();

        //FileCrawler fileCrawler = new FileCrawler();
        fileCrawler = new FileCrawler();
        fileCrawler.execute();
    }


    /**
     * Reload folders and refresh MainView
     */
    public void refreshViewContents() {
        new AddThumbnailsMainView().execute(this.folders);
    }





    /**
     * Asynctask to add thumbnails to the gridview
     */
    private class AddThumbnailsMainView extends AsyncTask<List<PictureFolder>, Void, Integer> {

        protected Integer doInBackground(List<PictureFolder>... params) {
            List<PictureFolder> folders = params[0];
            ArrayList<String> imagePaths = new ArrayList<>();

            // updating UI from Background Thread
            for (PictureFolder folder : folders) {
                imagePaths.add(folder.getPicturePaths().get(0));

                if (isCancelled()) {
                    break;
                }
            }

            imageAdapter.addThumbs(imagePaths);

            return imagePaths.size();
        }
    }









    /**
     * Asynctask to search through the filesystem for pictures and adds Thumbs to the MainView
     */
    public class FileCrawler extends AsyncTask<Void, PictureFolder, Boolean> {

        /**
         * Crawl through the file system searching for appropriate objects
         */
        protected Boolean doInBackground(Void... unused) {
            // check, if external Storage is available
            if (!isExternalStorageReadable()) {
                return true;
            }

            //File rootFolder = Environment.getExternalStorageDirectory();
            File rootFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

            findFolders(rootFolder);

            return false;
        }


        protected void onProgressUpdate(PictureFolder... newFolders) {
            for (PictureFolder folder : newFolders) {
                folders.add(folder);
                imageAdapter.addThumb(folder.getPicturePaths().get(0));
            }
            imageAdapter.notifyDataSetChanged();
        }


        protected void onPostExecute(Boolean storageError) {
            if (storageError) {
                Toast.makeText(MainViewActivity.this, R.string.error_storage_not_readable, Toast.LENGTH_LONG).show();
            }
        }


        /**
         * Go through the given folder and add folders with pictures
         * @param currentFolder
         */
        private void findFolders(File currentFolder) {

            if (isCancelled()) {
                return;
            }


            FilenameFilter imageFilter = new FilenameFilter() {
                File f;
                public boolean accept(File dir, String name) {

                    // look, if file is supported
                    for (String type : GlobalConstants.SUPPORTED_FILE_TYPES) {
                        if(name.toLowerCase().endsWith(type) &&
                                !name.startsWith(".")) {    // ignore hidden files
                            return true;
                        }
                    }

                    // if it's an directory, do a recursive call
                    File file = new File(dir.getAbsolutePath()+"/"+name);
                    if (file.isDirectory()) {
                        findFolders(file);
                    }

                    return false;
                }
            };

            String[] images = currentFolder.list(imageFilter); // get list of suitable object names

            // if there are suitable objects in this folder, add it to the list
            if (images.length > 0) {
                PictureFolder folder = new PictureFolder(currentFolder);

                String currentPath = folder.getFolder().getAbsolutePath();
                for (String imageName : images) {
                    folder.addPicture(currentPath + "/" + imageName);
                }

                publishProgress(folder);
            }
        }


        /**
         *  Checks if external storage is available to at least read
         **/
        public boolean isExternalStorageReadable() {
            String state = Environment.getExternalStorageState();
            if ((Environment.MEDIA_MOUNTED.equals(state) ||
                    Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)
                )
                    && PermissionManager.isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                return true;
            }
            return false;
        }

    }
}
