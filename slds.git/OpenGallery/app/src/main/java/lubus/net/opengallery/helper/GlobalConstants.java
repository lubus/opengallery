/*
* Copyright 2015 Eicke Hauck
*
* This file is part of OpenGallery.
*
* OpenGallery is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* OpenGallery is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenGallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package lubus.net.opengallery.helper;

/**
 * Created by eicke on 04.12.15.
 * Provide global constants
 */
public class GlobalConstants {
    // Number of columns of Grid View
    public static final int NUM_OF_COLUMNS = 3;

    // Gridview image padding
    public static final int GRID_PADDING = 8; // in dp

    // supported file formats
    public static final String[] SUPPORTED_FILE_TYPES = {".png", ".jpg", ".jpeg"};
}
