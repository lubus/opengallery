/*
* Copyright 2015 Eicke Hauck
*
* This file is part of OpenGallery.
*
* OpenGallery is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* OpenGallery is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenGallery.  If not, see <http://www.gnu.org/licenses/>.
*/


package lubus.net.opengallery;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by eicke on 11.12.15.
 */
public class FolderImageAdapter extends MainImageAdapter {

    public FolderImageAdapter(Context c, int imageWidth) {
        super(c, imageWidth);
    }

    /**
     *Create a new ImageView for each item referenced by the Adapter
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(imageWidth,imageWidth));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }

        if (mThumbIds.size() > position) {
            imageView.setImageBitmap(mThumbIds.get(position));
        }

        return imageView;
    }

}
