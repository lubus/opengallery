/*
* Copyright 2015 Eicke Hauck
*
* This file is part of OpenGallery.
*
* OpenGallery is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* OpenGallery is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenGallery.  If not, see <http://www.gnu.org/licenses/>.
*/


package lubus.net.opengallery;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import lubus.net.opengallery.helper.GlobalMethods;

public class PictureViewActivity extends AppCompatActivity {

    File picture;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.content_picture_view);

        // receive Intent
        Intent intent = getIntent();
        this.picture = new File(intent.getStringExtra(FolderViewActivity.EXTRA_PICTURE_PATH));


        ImageView imgDisplay = (ImageView) findViewById(R.id.imgDisplay);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picture.getAbsolutePath(), options);

        // calculate required widht and height
        int originalHeight = options.outHeight;
        int originalWidth = options.outWidth;
        int requiredWidth = this.getResources().getDisplayMetrics().widthPixels;
        int requiredHeight = originalHeight/originalWidth * requiredWidth; // preserve the image ratio

        // get sample and show the image
        bitmap = GlobalMethods.getSampleBitmapFromFile(picture.getAbsolutePath(), requiredWidth, requiredHeight);
        imgDisplay.setImageBitmap(bitmap);

    }

    @Override
    protected void onStop() {
        bitmap = null;
        System.gc();
        super.onStop();
    }
}
