/*
* Copyright 2015 Eicke Hauck
*
* This file is part of OpenGallery.
*
* OpenGallery is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* OpenGallery is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenGallery.  If not, see <http://www.gnu.org/licenses/>.
*/


package lubus.net.opengallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import lubus.net.opengallery.helper.GlobalMethods;

/**
 * Created by eicke on 11.12.15.
 */
public class MainImageAdapter extends BaseAdapter {

    Context mContext;
    int imageWidth;
    ThumbnailsAdderTask thumbnailsAdderTask = new ThumbnailsAdderTask();
    LayoutInflater inflater = null;

    ArrayList<Bitmap> mThumbIds = new ArrayList<>(); // references to the images
    ArrayList<String> mCaptionIds = new ArrayList<>(); // references to the captions


    public final int THUMBSIZE = 100; // change this to configure the resolution of the thumbnails


    public MainImageAdapter(Context c, int imageWidth) {
        mContext = c;
        this.imageWidth = imageWidth;
        this.inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount(){
        return mThumbIds.size();
    }


    public Object getItem(int position) {
        return this.mThumbIds.get(position);
    }

    // todo
    @Override
    public long getItemId(int position) {
        return position;
    }


    /**
     * Class, which saves the content of the used views
     */
    public static class ViewHolder {
        TextView tv;
        ImageView img;
    }

    /**
     *Create a new ImageView for each item referenced by the Adapter
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        View view = convertView;

        if (view == null) {
            // if it's not recycled, initialize some attributes
            viewHolder = new ViewHolder();

            view = inflater.inflate(R.layout.my_image_view, null);

            viewHolder.img = (ImageView) view.findViewById(R.id.imageView1);
            viewHolder.img.setLayoutParams(new LinearLayout.LayoutParams(imageWidth, imageWidth));
            viewHolder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);

            viewHolder.tv = (TextView) view.findViewById(R.id.textView1);
            viewHolder.tv.setLines(1);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if (mThumbIds.size() > position) {
            viewHolder.img.setImageBitmap(mThumbIds.get(position));
            viewHolder.tv.setText(mCaptionIds.get(position));
        }

        return view;
    }


    /**
     * Wipe the cached Thumbs
     */
    public void resetThumbs() {
        this.mThumbIds.clear();
    }


    /**
     * Add Thumb to the MainView
     * @param imagePath File, where the image is stored. It will be
     *             converted to a Bitmap and added to the MainView
     */
    public void addThumb(String imagePath) {
        thumbnailsAdderTask.addImage(imagePath);
        startThumbnailsAdderTask();
    }

    /**
     * Add several Thumbs at once
     * @param imagePaths File Paths to the images
     */
    public void addThumbs(List<String> imagePaths) {
        for (String path : imagePaths) {
            thumbnailsAdderTask.addImage(path);
        }
        startThumbnailsAdderTask();
    }


    /**
     * Ensures, that the thumbnailsAdderTask is running. If not, start it.
     */
    public void startThumbnailsAdderTask() {
        if (thumbnailsAdderTask.getStatus() != AsyncTask.Status.RUNNING) {
            try {
                thumbnailsAdderTask.execute();
            } catch (IllegalStateException e) {
                thumbnailsAdderTask = new ThumbnailsAdderTask();
                thumbnailsAdderTask.execute();
            }
        }
    }


    /**
     * Asynctask to add thumbnails to the gridview
     */
    private class ThumbnailsAdderTask extends AsyncTask<Void, NamedImage, Void> {

        private ArrayList<String> imagePaths = new ArrayList<>();

        protected void addImage(String imagePath) {
            this.imagePaths.add(imagePath);
        }

        protected Void doInBackground(Void... unused) {
            String imagePath; Bitmap bmp; String[] parts; String caption;

            // Extract thumbnails and add them to the UI with caption
            while (!imagePaths.isEmpty()) {
                imagePath = imagePaths.remove(0);
                bmp = GlobalMethods.getSampleBitmapFromFile(imagePath, THUMBSIZE, THUMBSIZE);
                parts = imagePath.split("/");
                caption = parts[parts.length-2];

                publishProgress(new NamedImage(bmp, caption)); // Thumbnail has to be added in this method

                if (isCancelled()) {
                    break;
                }
            }

            return null;
        }

        protected void onProgressUpdate(NamedImage... namedImages) {
            mThumbIds.add(namedImages[0].bmp);
            mCaptionIds.add(namedImages[0].caption);

            notifyDataSetChanged(); // updates gridView
        }
    }

    private class NamedImage {
        Bitmap bmp;
        String caption;

        public NamedImage(Bitmap bmp, String caption) {
            this.bmp = bmp;
            this.caption = caption;
        }
    }

}

