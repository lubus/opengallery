/*
* Copyright 2015 Eicke Hauck
*
* This file is part of OpenGallery.
*
* OpenGallery is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* OpenGallery is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenGallery.  If not, see <http://www.gnu.org/licenses/>.
*/


package lubus.net.opengallery;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by eicke on 02.12.15.
 * Folder of the file system with pictures in it
 */
public class PictureFolder implements Parcelable {
    private File folder;
    private ArrayList<String> picturePaths = new ArrayList<>();

    public PictureFolder(File folder) {
        if (folder.isDirectory() == false) {
            throw new IllegalArgumentException("Given file is not a directory!");
        }
        this.folder = folder;
    }

    public PictureFolder(File folder, ArrayList<String> picturePaths) {
        this(folder);
        this.picturePaths = picturePaths;
    }


    protected PictureFolder(Parcel parcel) {
        this.folder = (File) parcel.readValue(File.class.getClassLoader());

        for (Object o : parcel.readArray(String[].class.getClassLoader())) {
            this.picturePaths.add((String) o);
        }
    }

    public static final Creator<PictureFolder> CREATOR = new Creator<PictureFolder>() {
        @Override
        public PictureFolder createFromParcel(Parcel in) {
            return new PictureFolder(in);
        }

        @Override
        public PictureFolder[] newArray(int size) {
            return new PictureFolder[size];
        }
    };

    public File getFolder() {
        return folder;
    }

    public ArrayList<String> getPicturePaths() {
        return picturePaths;
    }

    public void addPicture(String picture) {
        this.picturePaths.add(picture);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(folder);
        dest.writeArray(picturePaths.toArray());
    }
}
