/*
* Copyright 2015 Eicke Hauck
*
* This file is part of OpenGallery.
*
* OpenGallery is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* OpenGallery is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with OpenGallery.  If not, see <http://www.gnu.org/licenses/>.
*/


package lubus.net.opengallery.helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.GridView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import lubus.net.opengallery.MainViewActivity;

/**
 * Created by eicke on 04.12.15.
 * Provides methods used in the whole projects
 */
public class GlobalMethods {


    public static int getScreenWidth(Context context) {
        int columnWidth;
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        return columnWidth;
    }

    /**
     * Initializes a given grid view. Returns the columnWidth of the gridview
     * @param gridview
     */
    public static int initializeGridLayout(Context context, GridView gridview) {
        Resources r = context.getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                GlobalConstants.GRID_PADDING, r.getDisplayMetrics());

        int columnWidth = (int) ((GlobalMethods.getScreenWidth(context) - ((GlobalConstants.NUM_OF_COLUMNS + 1) * padding)) / GlobalConstants.NUM_OF_COLUMNS);

        gridview.setNumColumns(GlobalConstants.NUM_OF_COLUMNS);
        gridview.setColumnWidth(columnWidth);
        gridview.setStretchMode(GridView.NO_STRETCH);
        gridview.setPadding((int) padding, (int) padding, (int) padding,
                (int) padding);
        gridview.setHorizontalSpacing((int) padding);
        gridview.setVerticalSpacing((int) padding);

        return columnWidth;
    }


    /**
     * Calculate appropriate sample size for given bitmap specs
     * @param options BitmapFactory.Options, which inherit the specs of the picture
     * @param reqWidth Size of the desired width
     * @param reqHeight Size of the desired height
     * @return The size of the sample
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float)height / (float)reqHeight);
            final int widthRatio = Math.round((float)width / (float)reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee a final image with both dimensions larger than
            // or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }


    /**
     * Get a Bitmap for a given filepath with a desired resolution
     * @param bitmapFilePath Path to the file
     * @param reqWidth Size of the desired width
     * @param reqHeight Size of the desired width
     * @return A bitmap of the desired resolution
     */
    public static Bitmap getSampleBitmapFromFile(String bitmapFilePath, int reqWidth, int reqHeight) {
        // calculating image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; // see documentation description

        try {
            BitmapFactory.decodeStream(new FileInputStream(new File(bitmapFilePath)), null, options); // get image specs

            int scale = calculateInSampleSize(options, reqWidth, reqHeight);

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;

            return BitmapFactory.decodeStream(new FileInputStream(new File(bitmapFilePath)), null, o2);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
